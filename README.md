Gateway Management
==================

Automatic provisioning and management scripts used to setup and remotely access gateways.

In Progress Tasks
-----------------

* Test the scripts by provisioning an actual gateway
* Correct any mistakes in the gateway scripts and adapt them to a new gateway
* Automate the installation of AWS Greengrass

