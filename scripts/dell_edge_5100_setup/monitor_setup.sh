#
# Created for the Dell Edge 5100 Gateway
# Gateway monitoring setup script
#

#!/usr/bin/env bash

SCRIPT_DIR=dirname "$0"
DATA_DIR=$SCRIPT_DIR/data

echo "Installing monit and copying configuration file\n"
apt install monit

cp $DATA_DIR/monitrc /etc/monit/monitrc
