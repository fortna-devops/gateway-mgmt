#
# Created for the Dell Edge 5100 Gateway
# Setup security policies for the gateway
#

#!/usr/bin/env bash

SCRIPT_DIR=dirname "$0"
DATA_DIR=$SCRIPT_DIR/data

# Get the user password
read -p "Please enter the user password: " USER_PASSWORD

if [ -z "$USER_PASSWORD" ]; then
    echo "[ERROR] - Script needs to set the user password\n"
    exit 1
fi

echo "$USER_PASSWORD $USER_PASSWORD" | passwd mhs-predict

# Restrict core dumps
echo "* hard core 0" >> /etc/security/limits.conf
echo "root hard core 0" >> /etc/security/limits.conf

echo "fs.suid_dumpable = 0" >> /etc/sysctl.conf

echo "DumpCore=no" >> /etc/systemd/system.conf

sysctl -w fs.suid_dumpable=0

cp $DATA_DIR/rc.local /etc/rc.local

chmod +x /etc/rc.local
systemctl enable rc-local

# Restrict access to 'su' command
auth required pam_wheel.so use_uid

sed -i 's/wheel:x:10:root/wheel:x:10:root,mhs-predict/g' /etc/group

# Protect the SSH server
chown root:root /etc/ssh/sshd_config
chmod og-rwx /etc/ssh/sshd_config

cat sshd_config.data >> /etc/ssh/sshd_config

# Disable extra filesystem support
cp $DATA_DIR/CIS.conf /etc/modprobe.d/CIS.conf

echo "tmpfs /dev/shm tmpfs defaults,nodev,nosuid,noexec 0 0" >> /etc/fstab
