#
# Created for the Dell Edge 5100 Gateway
#

#!/usr/bin/env bash

# Get the grub password
read -p "Please enter the grub password: " GRUB_PASSWORD

if [ -z "$GRUB_PASSWORD" ]; then
    echo "[ERROR] - Script needs grub password to take ownership\n"
    exit 1
fi

GRUB_HASH = $(echo "$GRUB_PASSWORD $GRUB_PASSWORD" | grub-mkpasswd-pbkdf2)

echo "set superusers=\"mhs\"" >> /etc/grub.d/40_custom
echo "password_pbkdf2 mhs $GRUB_HASH" >> /etc/grub.d/40_custom

sed -i 's/--class os/--class os --unrestricted/g' /etc/grub.d/10_linux

update-grub