#
# Initial TPM setup script for Ubuntu 18.04.1
# Created for the Dell Edge 5100 Gateway
#

#!/usr/bin/env bash

SCRIPT_DIR=$(dirname "$0")
DATA_DIR=$SCRIPT_DIR/data

TPM_SOFTWARE_VERSION="2.0.0"
TPM_ABRMD_VERSION="2.0.0"
TPM_TOOLS_VERSION="3.1.0"

DISTRO_REQ="Ubuntu 18.04.1 LTS"
DISTRO_REGEX="DISTRIB_DESCRIPTION=\"(.+)\""
DISTRO_INFO=$(cat /etc/*-release)

[[ DISTRO_INFO =~ DISTRO_REGEX ]]

# Check to see if this script is running on the expected distro
if [ "${BEST_REMATCH[1]}" == "${DISTRO_REQ}" ]; then
    echo "[ERROR] - Failed to run provisioning script: wrong OS\n"
    exit 1
fi

# Check to see if we have root privileges
if [[ $EUID != 0 ]]; then
    echo "[ERROR] - Script must be run with sudo "$0" instead\n"
    exit 1
fi

# Get the default password
read -p "Please enter the default TPM password: " TPM_PASSWORD

if [ -z "$TPM_PASSWORD" ]; then
    echo "[ERROR] - Script needs default TPM password to take ownership\n"
    exit 1
fi

# Get the main partition
read -e -p "Please enter the main partition:" -i "sda3" TPM_PARTITION

if [ -z "$TPM_PARTITION" ]; then
    echo "[ERROR] - Script needs main partition to place the secret key\n"
    exit 1
fi

# Install any pre-requisite packages
sudo apt -y install build-essential pkg-config gcc g++ m4 autoconf autoconf-archive automake libtool net-tools libssl-dev libcmocka0 libcmocka-dev uthash-dev libgcrypt20-dev libglib2.0-dev libdbus-1-dev libsapi-dev libcurl4-gnutls-dev

echo "Setup TPM steps...\n"

# Setup the user for TPM
useradd --system --user-group tss

# Install tpm2-software
cd $HOME
mkdir gateway_install
cd gateway_install

git clone https://github.com/tpm2-software/tpm2-tss.git
cd tpm2-tss 
git checkout $TPM_SOFTWARE_VERSION

./bootstrap
./configure --with-udevrulesdir=/etc/udev/rules.d

make -j8 install
ldconfig

udevadm control --reload-rules
udevadm trigger

# Install tpm2-abrmd
cd $HOME/gateway_install

git clone https://github.com/tpm2-software/tpm2-abrmd.git
cd tpm2-abrmd
git checkout $TPM_ABRMD_VERSION

./bootstrap
./configure --with-dbuspolicydir=/etc/dbus-1/system.d

make -j8 install
ldconfig

pkill -HUP dbus-daemon
systemctl daemon-reload

# Install tpm2-tools
cd $HOME/gateway_install

git clone https://github.com/tpm2-software/tpm2-tools.git
cd tpm2-tools
git checkout $TPM_TOOLS_VERSION

./bootstrap
./configure

make -j8 install
ldconfig

# Take ownership 
tpm2_takeownership -o $TPM_PASSWORD

# Create the secret key
dd bs=1 count=32 if=/dev/random of=/secret_key.bin
cryptsetup luksAddKey /dev/$TPM_PARTITION /secret_key.bin

# Clear TPM owner
tpm2_takeownership -c

# Save list of PCRs
tpm2_pcrlist -L sha1:0,2,3,7 -o pcrs.bin 

# Create PCR policy
tpm2_createpolicy -P -L sha1:0,2,3,7 -F pcrs.bin -f policy.digest

# Create primary TPM object
tpm2_createprimary -H e -g sha1 -G rsa -C primary.context

# Create TPM object with secret
tpm2_create -g sha256 -G keyedhash -u obj.pub -r obj.priv -c primary.context -L policy.digest -A "noda|adminwithpolicy|fixedparent|fixedtpm" -I /secret.bin

# Load object into the TPM
tpm2_load -c primary.context -u obj.pub -r obj.priv -C load.context

# Make TPM object persistent
tpm2_evictcontrol -A o -H 0x80000100 -S 0x81000100 -c load.context

# Move files that are needed for initramfs
cp $DATA_DIR/tpm_get_pass.sh /tpm_get_pass.sh
cp $DATA_DIR/tpm2.sh /etc/initramfs-tools/hooks/tpm2.sh

chmod +x /tpm_get_pass.sh
chmod +x /etc/initramfs-tools/hooks/tpm2.sh

# Make appropriate changes to crypttab
UUID=$(blkid -o value -s UUID /dev/$TPM_PARTITION)
$TPM_PARTITION_crypt UUID=$UUID none luks,keyscript=/tpm_get_pass.sh

update-initramfs -u
