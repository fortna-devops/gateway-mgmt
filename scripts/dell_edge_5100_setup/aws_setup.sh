#
# Created for the Dell Edge 5100 Gateway
# AWS setup script
#

#!/usr/bin/env bash

# Add Greengrass user
adduser --system ggc_user
addgroup --system ggc_group
usermod -aG dialout ggc_user
apt update && sudo apt install -y sqlite3
sysctl -a 2> /dev/null | grep fs.protected

# Install Greengrass dependencies
apt update
apt install -y python2.7 python python-pip python3-pip
add-apt-repository ppa:webupd8team/java
apt install -y oracle-java8-installer
curl -sL https://deb.nodesource.com/setup_8.x| sudo -E bash -
apt install -y nodejs
ln -s /usr/bin/java /usr/bin/java8
ln -s /usr/bin/nodejs /usr/bin/nodejs6.10
pip2 install minimalmodbus pySerial arrow

# Copy the service files over
cp greengrass.service /etc/systemd/system/greengrass.service

echo "Starting AWS Greengrass service\n"
systemctl enable greengrass

cp -r certs /opt/greengrass
cp -r config /opt/greengrass
wget https://www.symantec.com/content/en/us/enterprise/verisign/roots/VeriSign-Class%203-Public-Primary-Certification-Authority-G5.pem -O /opt/greengrass/certs/root.ca.pem
